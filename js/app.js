/* global angular */

angular.module('app', ['ionic', 'app.controllers', 'app.services'])

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
    })

    .state('turbine', {
        url: '/turbine',
        templateUrl: 'templates/turbine.html',
        controller: 'turbineCtrl'
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
})

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
      console.log("app ready");
  });
});
angular.module('app.services', [])

.factory('CompactSCADA', function($window){
    var logged = false;
    sessionStorage.setItem('key', 'value');
    return {
        isLogged: function(){return logged;},
        getUsername: function(){return $window.sessionStorage.getItem("username");},
        
        login: function(username){
            $window.sessionStorage.setItem("username", username);
            logged = true;
        },
        logout: function(){
            $window.sessionStorage.setItem("username", undefined);
            logged = false;
        }
    };
});


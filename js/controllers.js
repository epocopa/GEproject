angular.module('app.controllers', ['app.services'])

.controller('loginCtrl', function($scope, $location, CompactSCADA) {
    $scope.data = {
        username: CompactSCADA.getUsername()
    };
    $scope.login = function(username){
        CompactSCADA.login(username);
        $location.path('/turbine');
    }
})

.controller('turbineCtrl', function($scope) {


    $scope.getPower = function(windspeed){
        var power = (windspeed===0)? 0 : Math.log(windspeed)*500;
        return power.toFixed(2);
        };
    $scope.data = {
        windspeed : 0,
        status: '0K'
    }
    $scope.scale = 30;
    $scope.transformation = 2;
});
